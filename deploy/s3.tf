# https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/s3_bucket

resource aws_s3_bucket app_public_files {
  bucket = "${local.prefix}-files"

  # https://docs.aws.amazon.com/AmazonS3/latest/dev/acl-overview.html#canned-acl
  acl = "public-read"

  # WARNING! alllows terraform to destroy the bucket and all files inside without confirming 
  force_destroy = true
}