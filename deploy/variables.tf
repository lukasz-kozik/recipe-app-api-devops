variable "prefix" {
  type        = string
  default     = "read"
  description = "description"
}


variable "project" {
  default = "recipe-app-api-devops"
}

variable "contact" {
  default = "lukasz.kozik@asenti.pl"
}

variable "db_password" {
  description = "Username for the RDS Postgres instance"
}

variable "db_username" {
  description = "Password for the RDS Postgres instance"
}

variable "bastion_key_name" {
  default = "recipe-app-api-devops-bastion"
}

variable "ecr_image_api" {
  description = "ECR image for API"
  default     = "337736875251.dkr.ecr.eu-central-1.amazonaws.com/recipe-app-api-devops:latest"
}

variable "ecr_image_proxy" {
  description = "ECR image for PROXY"
  default     = "337736875251.dkr.ecr.eu-central-1.amazonaws.com/recipe-app-api-proxy:latest"
}

variable "django_secret_key" {
  description = "secret key for Django app"
}
